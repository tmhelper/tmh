# core function with shorthand "_"
import topic_model_helper_core as _
from datetime import datetime

scriptStart = datetime.now()

# Input/Output Options:
_.PATH_FILES = "C:\\yourfolder\\topic model\\" # Path of input and output csv/txt files
_.CORPUS_SRC = "corpus example - input.csv"

# File Format:
# "Datetime - DTM-Package NUM_TOPICS filename.csv"
# "2018_02_23 16_55_06 - TM tpc100 chart10.csv"

_.SAFE_WORDCHART = 1 # Store Top-n-words       to CSV? (0:No | 1:Yes)
_.SAFE_TOPICWORD = 1 # Store Topic-Word-Matrix to CSV? (0:No | 1:Yes)
_.SAFE_DOCSTOPIC = 1 # Store Topic- Doc-Matrix to CSV? (0:No | 1:Yes)

_.WORDCHART_CNT         = 10 # Safe first x Words of each topic to Wordchart
_.WORDCHART_FREQ_ABS    = 1  # Display absolute Frequency of each chart-word (0:No | 1:Yes)
_.WORDCHART_FREQ_REL    = 0  # Display relative Frequency of each chart-word (0:No | 1:Yes)

_.TOPICWORD_FREQ_REL    = 0  # Rel. Values in Topic-Word-Matrix? 1: relative(percent) | 0: absolute
_.DOCSTOPIC_FREQ_REL    = 0  # Rel. Values in Topic-Docs-Matrix? 1: relative(percent) | 0: absolute

# DTM Options:
_.N_TOPICS = 10  # number of Topics
_.N_ITER   = 10  # number of iterations, higher value more stable results (best: 100-1000)

# DTM Options: GuidedLDA-Specific:
_.PY_ALPHA       = 1    # Hyperparameter alpha, best: 1
_.PY_ETA         = 0.1  # TM-ONLY: Hyperparameter eta, best: 0.1 (don't confuse with beta)
_.PY_REFRESH     = 10   # refresh loglikelihood every n iterations
_.PY_USE_SEEDS   = 1    # Use imported seedlist? (0:No | 1:Yes)
_.PY_SEED_CONFID = 0.8  # Seed confidency lvl from 0.001 bis 1
_.PY_PRNT_TPCS   = 0    # Print topic-Results to console?

# Seedlist format: "seed1word1;seed1word2;seed1word3[\n]seed2word1;seed2word2;seed2word3[\n]"
# Order of words is not relevant. Order of seeds is not relevant. Always end file with Newline
_.FILE_SEEDS = "seedwords example.csv" # File from where the seedlist will be imported

_.LDADE_min_alpha = 0.001 # min alpha Hyperparameter value
_.LDADE_max_alpha = 1     # max alpha Hyperparameter value
_.LDADE_min_beta  = 0.001 # min beta  Hyperparameter value
_.LDADE_max_beta  = 1     # max beta  Hyperparameter value
_.LDADE_min_tpcs  = 10    # number of Topics to be tested (min and max should be identic)
_.LDADE_max_tpcs  = 10    # number of Topics to be tested (min and max should be identic)

# ----------------------------
# FUNCTIONS:
# ----------------------------
# _.importCorpus()        # Import prepared corpus from CSV-File (CORPUS_SRC). Format: "ID[\t]isUni[\t]doc_text[\n]"
# _.runDTM_withSettings() # DTM-Function, can be runned multiple times
# _.exportCorpus()        # Export prepared Corpus to csv-File for Mallet-Console input. Format: "ID[\t]isUni[\t]doc_text[\n]"



# -------------------------------------------
# WORKSPACE: place functions / changed settings here
# -------------------------------------------
if __name__ == '__main__':

    #_.LDADE_main(p_iterdtm=10) # this needs a while to process

    _.importCorpus()
    _.runDTM_withSettings()
    _.exportCorpus()

print("===========================================")
print("Script successfully finished. Execution time: " + str(datetime.now()-scriptStart))