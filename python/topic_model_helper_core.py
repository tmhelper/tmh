import numpy as np
import guidedlda
import lda
import random
import gensim
import nltk
import sys
from nltk.tokenize import RegexpTokenizer
from gensim import corpora
from datetime import datetime
from operator import itemgetter
from concurrent.futures import ProcessPoolExecutor, as_completed, wait

PATH_FILES            = None
CORPUS_SRC            = None
SAFE_WORDCHART        = None
SAFE_TOPICWORD        = None
SAFE_DOCSTOPIC        = None
WORDCHART_CNT         = None
WORDCHART_FREQ_ABS    = None
WORDCHART_FREQ_REL    = None
TOPICWORD_FREQ_REL    = None
DOCSTOPIC_FREQ_REL    = None
N_TOPICS              = None
N_ITER                = None
PY_ALPHA              = None
PY_ETA                = None
PY_REFRESH            = None
PY_USE_SEEDS          = None
PY_SEED_CONFID        = None
PY_PRNT_TPCS          = None
FILE_SEEDS            = None


corpDic     = []
corpDic2Id  = []
dtm_matrix  = []

# new:
dokMatrix   = []
seedList    = []
chartList   = []

LDADE_min_alpha = 0.001
LDADE_max_alpha = 1
LDADE_min_beta  = 0.001
LDADE_max_beta  = 1
LDADE_min_tpcs  = 170
LDADE_max_tpcs  = 170


def crtDictAndDocTermMatrx():

    global corpDic
    global corpDic2Id
    global dtm_matrix

    corpDic    = []
    corpDic2Id = []
    arrDocBags = []

    objTokenize = RegexpTokenizer(r'\w+')

    for i in range(0, len(dokMatrix)):
        doc_text = dokMatrix[i][2]
        doc_tokens = objTokenize.tokenize(doc_text)
        arrDocBags.append(doc_tokens)


    objDict = corpora.Dictionary(arrDocBags)

    numWordsUnq = len(objDict)
    numDocs     = len(dokMatrix)

    for i in range(0, numWordsUnq):
        corpDic.append(objDict[i])

    corpDic2Id = dict((v, idx) for idx, v in enumerate(corpDic))

    # convert tokenized documents into a document-term matrix
    objCorpus = [objDict.doc2bow(text) for text in arrDocBags]

    # rebuilt dtm matrix from objCorpus for guidedLda
    # warning: words with zero freq must be added manually!
    # [document][word_idx][0:dict_id|1:dict_count]

    dtm_matrix = [[] for i in range(numDocs)]

    for i in range(0, numDocs):

        lastWordId = -1
        wordLen = len(objCorpus[i])

        for j in range(0, wordLen):
            wordId  = objCorpus[i][j][0]
            wordCnt = objCorpus[i][j][1]

            wordIdDiff = wordId - lastWordId

            if abs(wordIdDiff) > 1:
                for k in range(0, wordIdDiff - 1):
                    dtm_matrix[i].append(0)

            dtm_matrix[i].append(wordCnt)
            lastWordId = wordId

        wordToEndDiff = numWordsUnq - (objCorpus[i][wordLen - 1][0] + 1)
        if abs(wordToEndDiff) > 0:
            for m in range(0, wordToEndDiff):
                dtm_matrix[i].append(0)

def runDTM():

    crtDictAndDocTermMatrx()

    # model.doc_topic_  document-topic distributions
    # model.topic_word_ topic word distributions

    # X[1] - X[n]: n Number of Documents
    # X[23][1] - X[23][n] show all wordfreqs for document 23
    # X.sum() summarize all entries of matrix gives total amount of words
    # corpDic2Id: add Id's to vocabulary:  {'church': 0, 'pope': 1, 'years': 2, 'people': 3}

    dtm_matrix_np = np.array(dtm_matrix, dtype=np.intc)

    model = guidedlda.GuidedLDA(n_topics=N_TOPICS, n_iter=N_ITER, refresh=PY_REFRESH)#, random_state=9973)

    if PY_USE_SEEDS == 1:
        seed_topics = {}
        for t_id, st in enumerate(seedList):
            for word in st:
                seed_topics[corpDic2Id[word]] = t_id

        model.fit(dtm_matrix_np, seed_topics=seed_topics, seed_confidence=PY_SEED_CONFID)

    else:
        model.fit(dtm_matrix_np)

    abs_topic_word = model.nzw_        # topic-word Matrix abs val [topic][word]
    abs_doc_topic  = model.ndz_        # doc-topic  Matrix abs val [doc][topic]


    if PY_PRNT_TPCS == 1:
        for i, topic_dist in enumerate(topic_word):
            topic_words = np.array(corpDic)[np.argsort(topic_dist)][:-(WORDCHART_CNT+1):-1]
            print('Topic {}: {}'.format(i, ' '.join(topic_words)))

    if SAFE_WORDCHART == 1:
        arrLines = []
        filePath = PATH_FILES + ROOT_FILE + " chart" + str(WORDCHART_CNT) + ".csv"

        freq_sep = ";"

        # CSV-Header:
        header = "TopicNr"
        for i in range(0, WORDCHART_CNT):

            currData = "#" + str(i+1) # no topic0!

            if WORDCHART_FREQ_ABS == 1: currData = currData + freq_sep + "freqAbs"
            if WORDCHART_FREQ_REL == 1: currData = currData + freq_sep + "freqRel"

            header = header + ";" + currData
        arrLines.append((header + "\n"))

        # CSV-Body:
        # iterate through topics
        for i in range(0, N_TOPICS):

            currTopic = str(i+1)  # no topic0!
            freqTotal = 0

            currLine = "Topic" + currTopic

            currT_words = [] # format: [wordNr][0]: wordName | [wordNr][1]: wordFreq
            for k in range(0, len(corpDic)):
                currT_words.append([corpDic[k], abs_topic_word[i][k]])

            currT_words = sorted(currT_words, key=itemgetter(1), reverse=True)

            # summarize all freqs of one topic
            for k in range(0, len(currT_words)):
                currFreq  = currT_words[k][1]
                freqTotal = freqTotal + currFreq

            # iterate through top n terms
            for k in range(0, WORDCHART_CNT):

                currWord    = currT_words[k][0]
                currFreqAbs = currT_words[k][1]

                currFreqRel = getFreqRel(currFreqAbs, freqTotal)

                currData = currWord

                if WORDCHART_FREQ_ABS == 1: currData = currData + freq_sep + str(currFreqAbs)
                if WORDCHART_FREQ_REL == 1: currData = currData + freq_sep + str(currFreqRel)

                currLine = currLine + ";" + currData

            arrLines.append((currLine + "\n"))

        writeLines2File(filePath, arrLines)
        print("CSV: safed Word-Chart")


    if SAFE_TOPICWORD == 1:
        arrLines = []
        filePath = PATH_FILES + ROOT_FILE + " tpcword.csv"

        # CSV-Header
        header = "TopicNr"
        for k in range(0, len(corpDic)):
            header = header + ";" + str(corpDic[k])
        arrLines.append((header + "\n"))

        # CSV-Body:
        # iterate through topics
        for i in range(0, len(abs_topic_word)):

            currTopic = str(i+1) # no topic0!
            freqTotal = 0

            currLine = "Topic" + currTopic

            # summarize all freqs of one topic
            for j in range(0, len(abs_topic_word[i])):
                currFreq  = abs_topic_word[i][j]
                freqTotal = freqTotal + currFreq

            # iterate through terms
            for j in range(0, len(abs_topic_word[i])):

                currFreqAbs = abs_topic_word[i][j]
                currFreq    = str(currFreqAbs)

                if TOPICWORD_FREQ_REL == 1:
                    currFreq = getFreqRel(currFreqAbs, freqTotal)

                currLine = currLine + ";" + currFreq

            arrLines.append((currLine + "\n"))

        writeLines2File(filePath, arrLines)
        print("CSV: safed Topic-Word-Matrix")


    if SAFE_DOCSTOPIC == 1:
        arrLines = []
        filePath = PATH_FILES + ROOT_FILE + " docstpc.csv"

        # CSV-Header
        header = "Doc-ID"
        for i in range(0, N_TOPICS):
            currTopic = str(i+1)  # no topic0!
            header = header + ";" + "Topic" + currTopic
        arrLines.append((header + "\n"))

        # CSV-Body:
        # iterate through documents
        for i in range(0, len(abs_doc_topic)):

            docId = dokMatrix[i][0]
            freqTotal = 0

            currLine = str(docId)

            # summarize all freqs of one document
            for j in range(0, len(abs_doc_topic[i])):
                currFreq  = abs_doc_topic[i][j]
                freqTotal = freqTotal + currFreq

            # iterate through all topics of one document
            for j in range(0, len(abs_doc_topic[i])):

                currFreqAbs = abs_doc_topic[i][j]
                currFreq    = str(currFreqAbs)

                if DOCSTOPIC_FREQ_REL == 1:
                    currFreq = getFreqRel(currFreqAbs, freqTotal)

                currLine = currLine + ";" + currFreq

            arrLines.append((currLine + "\n"))

        writeLines2File(filePath, arrLines)
        print("CSV: safed Doc-Topic-Matrix")


def getFreqRel(freqAbs, freqTotal):

    if freqTotal == 0:
        return (0)

    freqRel = freqAbs / freqTotal

    if freqRel != 0:
        freqRel = format(freqRel, '.15f')
    else:
        freqRel = "0"

    return (freqRel)


def loadSeedFile():

    global seedList
    seedList = []

    if PY_USE_SEEDS == 0:
        return

    filePath = PATH_FILES + FILE_SEEDS
    file_con = open(filePath, "r")

    lines = file_con.read().splitlines()

    for i in range(0, len(lines)):
        currLine = lines[i].split(';')
        seedList.append(currLine)

    file_con.close()


def importCorpus():

    global dokMatrix
    global CORPUS_SRC
    dokMatrix = []

    fileName = CORPUS_SRC

    lines = []

    filePath = PATH_FILES + fileName
    file_con = open(filePath, "r")

    lines = file_con.read().splitlines()

    for i in range(0, len(lines)):

        arrLine = lines[i].split('\t')
        dokMatrix.append(arrLine)

    file_con.close()


def exportCorpus():

    createFileRoot()

    arrLines = []
    filePath = PATH_FILES + ROOT_FILE + " corpus.csv"

    for i in range(0, len(dokMatrix)):

        currID  = dokMatrix[i][0]
        currSrc = dokMatrix[i][1]
        currDok = dokMatrix[i][2]

        currLine = currID + "\t" + currSrc + "\t" + currDok + "\n"
        arrLines.append(currLine)

    writeLines2File(filePath, arrLines)
    print("CSV: exported Corpus")

def createFileRoot():

    global ROOT_FILE

    # Format:
    # "2018_02_23 16_55_06 - TM tpc100 chart10.csv"

    seedTag = "seed_no"
    if PY_USE_SEEDS == 1:
        seedTag = "seed_yes"

    currDateTime = str(datetime.now())
    currDateTime = currDateTime.split('.')[0]
    currDateTime = currDateTime.replace("-", "_")
    currDateTime = currDateTime.replace(":", "_")

    ROOT_FILE = currDateTime + " - PY " + seedTag + " tpc" + str(N_TOPICS)


def writeLines2File (filePath, arrLines):

    file_con = open(filePath, "a")
    file_con.writelines(arrLines)
    file_con.close()


def runDTM_withSettings():
    loadSeedFile()
    createFileRoot()
    runDTM()

# LDADE optimal alpha, beta, ntopic finding

# Compare two charts
def LDADE_getSimScore(arrChart_src, arrChart_tgt):

    n_scrTopics = len(arrChart_src)
    n_tgtTopics = len(arrChart_tgt)

    n_srcWords  = len(arrChart_src[0])
    n_tgtWords  = len(arrChart_tgt[0])

    score_nWord_sum = 0
    score_wFreq_sum = 0

    # iterate through all topics of src
    for i in range(0, n_scrTopics):

        score_nWord_best = 0
        score_wFreq_best = 0

        # iterate through all topics of tgt
        for k in range(0, n_tgtTopics):

            score_nWord_k = 0
            score_wFreq_k = 0

            # iterate through all words of src topic i
            for j in range(0, n_srcWords):

                src_word = arrChart_src[i][j][0]
                src_freq = arrChart_src[i][j][1]

                # iterate through all words of tgt topic k
                for l in range(0, n_tgtWords):

                    tgt_word = arrChart_tgt[k][l][0]
                    tgt_freq = arrChart_tgt[k][l][1]

                    if src_word == tgt_word:

                        score_nWord_k = score_nWord_k + 1

                        distFreq = abs(src_freq - tgt_freq)
                        score_wFreq_k = (score_wFreq_k + 1) - (distFreq)
                        break

            score_nWord_k = score_nWord_k / n_srcWords
            score_wFreq_k = score_wFreq_k / n_srcWords

            if score_nWord_k > score_nWord_best: score_nWord_best = score_nWord_k
            if score_wFreq_k > score_wFreq_best: score_wFreq_best = score_wFreq_k


        score_nWord_sum = score_nWord_sum + score_nWord_best
        score_wFreq_sum = score_wFreq_sum + score_wFreq_best

    scoreAvg = score_wFreq_sum / n_scrTopics
    return scoreAvg

# compare List of charts
def LDADE_compareEachRun():

    score_sum = 0
    compCount = 0

    for i in range(0, len(chartList)):
        for j in range(i+1, len(chartList)):

            score_curr = LDADE_getSimScore(chartList[i], chartList[j])
            score_sum  = score_sum + score_curr

            compCount  = compCount +1

    scoreAvg = score_sum / compCount
    return scoreAvg

def LDADE_createRun(p_alpha, p_beta, p_numTopic, p_iterdtm):

    global corpDic
    global corpDic2Id
    global dtm_matrix
    global dokMatrix

    corpDic    = []
    corpDic2Id = []
    dtm_matrix = []
    dokMatrix  = []

    importCorpus()

    random.shuffle(dokMatrix)

    crtDictAndDocTermMatrx()

    dtm_matrix_np = np.array(dtm_matrix, dtype=np.intc)

    model = lda.LDA(alpha=p_alpha, eta=p_beta, n_topics=p_numTopic, n_iter=p_iterdtm)
    model.fit(dtm_matrix_np)

    abs_topic_word = model.nzw_        # topic-word Matrix abs val [topic][word]

    thisChart = []

    # iterate through topics
    for i in range(0, p_numTopic):

        thisTopic = []

        freqTotal = 0

        currT_words = [] # format: [wordNr][0]: wordName | [wordNr][1]: wordFreq
        for k in range(0, len(corpDic)):
            currT_words.append([corpDic[k], abs_topic_word[i][k]])

        currT_words = sorted(currT_words, key=itemgetter(1), reverse=True)

        # summarize all freqs of Top10 words
        for k in range(0, 10):
            currFreq  = currT_words[k][1]
            freqTotal = freqTotal + currFreq

        # iterate through top n terms
        for k in range(0, 10):

            currWord     = currT_words[k][0]
            currFreqAbs  = currT_words[k][1]

            currFreqRel = currFreqAbs / freqTotal

            thisTopic.append([currWord, currFreqRel])

        thisChart.append(thisTopic)

    chartList.append(thisChart)
    return thisChart

def LDADE_getAvgScore(p_alpha, p_beta, p_numTopic, p_iterdtm, runMsg):

    global chartList

    score_sum = 0
    numRepeat = 7

    for i in range(0, numRepeat):

        chartList = []

        # multi-processing
        pool = ProcessPoolExecutor(1)
        futures = []

        for x in range(9):
            futures.append(pool.submit(LDADE_createRun, p_alpha, p_beta, p_numTopic, p_iterdtm))
            print(runMsg + " | numRepeat: " + str(i) + " | TenRunComp: " + str(x))

        wait(futures) # wait for all 10 runs to finish

        for x in as_completed(futures):
            chartList.append(x.result())

        print("comparing......")
        compEachRun = LDADE_compareEachRun()
        score_sum = score_sum + compEachRun
        print("comparing...... DONE. Score: " + str(compEachRun))

    scoreAvg = score_sum / numRepeat
    return scoreAvg

def LDADE_main(p_iterdtm):

    pop     = []
    currGen = []
    numGens = 3
    cr      = 0.3
    f       = 0.7

    arrLines = []
    filePath = PATH_FILES + "LDADE_results.csv"

    header = "numGen;alpha;beta;topic;sim_score\n"
    writeLines2File(filePath, header)

    # init Population:
    for i in range(0, 10):

        i_alpha  = random.uniform(LDADE_min_alpha, LDADE_max_alpha)
        i_beta   = random.uniform(LDADE_min_beta , LDADE_max_beta )
        i_nTopic = random.randint(LDADE_min_tpcs , LDADE_max_tpcs )

        pop.append([i_alpha, i_beta, i_nTopic])


    for i in range(0, len(pop)):

        runMsg = "Init: cand: -1 | a: " + str(pop[i][0]) + " | b: " + str(pop[i][1]) + " | t: " + str(pop[i][2])

        tempScore = LDADE_getAvgScore(pop[i][0], pop[i][1], pop[i][2], p_iterdtm, runMsg)
        currGen.append(           [pop[i][0], pop[i][1], pop[i][2], tempScore])

        currLine = "-1;" + str(pop[i][0]) + ";" + str(pop[i][1]) + ";" + str(pop[i][2]) + ";" + str(tempScore) + "\n"
        writeLines2File(filePath, currLine)

    for i in range(0, numGens):

        newGen = []

        for j in range(0, len(currGen)):

            runMsg = "Extpol: gen: " + str(i) + " | cand: " + str(j) + " | a: " + str(currGen[j][0]) + " | b: " + str(currGen[j][1]) + " | t: " + str(currGen[j][2])

            newTriple = LDADE_extrapolate(currGen, cr, f)

            if newTriple == False:
                newGen.append([currGen[j][0], currGen[j][1], currGen[j][2], currGen[j][3]])
            else:
                tempScore = LDADE_getAvgScore(newTriple[0], newTriple[1], newTriple[2], p_iterdtm, runMsg)

                if tempScore > currGen[j][3]:
                    newGen.append([newTriple[0], newTriple[1], newTriple[2], tempScore])
                else:
                    newGen.append([currGen[j][0], currGen[j][1], currGen[j][2], currGen[j][3]])

            currLine = str(i) + ";" + str(newGen[j][0]) + ";" + str(newGen[j][1]) + ";" + str(newGen[j][2]) + ";" + str(newGen[j][3]) + "\n"
            writeLines2File(filePath, currLine)

        currGen = newGen

    print(LDADE_getBestOfGen(currGen))



def LDADE_extrapolate(pop, cr, f):

    i_max = len(pop)-1

    x = pop[random.randint(0, i_max)]
    y = pop[random.randint(0, i_max)]
    z = pop[random.randint(0, i_max)]

    if cr <= random.random():
        return False

    else:
        x_new = x[0] + f * (y[0] - z[0])
        y_new = x[1] + f * (y[1] - z[1])
        z_new = x[2] + f * (y[2] - z[2])

        mutated = [x_new, y_new, int(z_new)]

        # check that new values are in valid range:
        check_mutated = [max(LDADE_min_alpha, min(mutated[0], LDADE_max_alpha  )),
                         max(LDADE_min_beta , min(mutated[1], LDADE_max_beta   )),
                         max(LDADE_min_tpcs , min(mutated[2], LDADE_max_tpcs   ))]

        return check_mutated


def LDADE_getBestOfGen(currGen):

    best_i     = 0
    best_score = 0

    for i in range(0, len(currGen)):

        currScore = currGen[i][3]

        if currScore > best_score:
            best_i     = i
            best_score = currScore

    return currGen[best_i]