# Topic Modeling Helper

This project is a collection of Rscript and Python scripts that contains
different topic modeling, topic quality measurement and 
improvements methods. With these tools you can run and compare topic
models from different packages. Furthermore you can determine
the optimal topic number and optimal values for hyperparameters
alpha and beta. With optional seedlists you can additionally improve
the quality of topics.
***
## Getting Started

Depending on which features you want to use, you need
to install either Rscript, Python or both.
We recommend development IDE's like RStudio or PyCharm to
view the Script-Codes, but it's optional.
Furthermore you need to install the packages listed below (or in the code).

For Python and rscript there is each:  
- a core File that you should not change  
- an interface file, where you can perform tasks and modify given parameters  

You can see how the algorithm works by viewing the given example input and
output files. Aditionally each parameter and function in the code is commented.
***
## Features

### RScript

- import Corpus-File as CSV  
=> Format: Document-ID[\t]Document-Class[\t]Document-Text[\n]  

- run Topic-Model with different Settings from TM or Mallet package  
=> with or without automatic Hyperparameter Adjustment, alpha, beta, number Iterations  

- run Quality test metrics from package ldatuning to determine number of topics  
=> metrics are: Griffiths2004, CaoJuan2009, Arun2010, Deveaud2014  

- run Cross-Perplexity topic test to determine number of topics  
=> with variable number of cross validations and topic candidates  

- export top-n chart of topic-word matrix to csv  
=> with their absolute and/or relative probabilities, different options  

- export the complete topic-word matrix to csv  
=> with their absolute and/or relative probabilities, different options  

- export document-topic matrix to csv  
=> output can be sorted by topic probability  

- run and export analytics for a single top-n chart of topic-word matrix  
=> measures inter-topic frequency, with score- or rank-weighting  
=> determines how exclusive a specific word and/or topic is based on word-appearances  

- run and export comparison of two top-n charts  
=> checks, how similar two Topic-Models are in regard to their Top-N topic-words  
=> similiraty score can be weighted with rank or probability  

### Python

- import Corpus-File as CSV  
=> Format: Document-ID[\t]Document-Class[\t]Document-Text[\n]  

- run Topic-Model with different Settings from lda or guidedLDA package  
=> with or without automatic Hyperparameter Adjustment, alpha, beta, number Iterations  
=> seedlists can be integrated to improve topic quality (guidedLDA)  

- run and export LDADE algorithm to determine best value for alpha and beta hyperparameters  
=> with adjustable settings (number of iterations, valid parameter range,...)  

## Examples
This project comes with an example corpus-file from [SHIVAM BANSAL](https://www.analyticsvidhya.com/blog/2016/08/beginners-guide-to-topic-modeling-in-python/) and [Jordan Barber](https://rstudio-pubs-static.s3.amazonaws.com/79360_850b2a69980c4488b1db95987a24867a.html/).

Sample output files of all features are also included.
***
## Built With

* Windows 7, 64 bit
* RScript Version 3.3.3
* RStudio Version 0.99.903
* Python 3.6.4
* PyCharm 2017.3.3 (Community)

We cannot guarantee, that the scripts will work under
other system and/or software circumstances.
***
## Used Packages
The code may not work/need adjustment using other package versions.

### Rscript
* [topicmodels 0.2-6](https://cran.r-project.org/web/packages/topicmodels/index.html)
* [lda 1.4.2](https://cran.r-project.org/web/packages/lda/index.html)
* [tm 0.7-1](https://cran.r-project.org/web/packages/tm/index.html)
* [mallet 1.0](https://cran.r-project.org/web/packages/mallet/index.html)
* [stringr 1.2.0](https://cran.r-project.org/web/packages/stringr/index.html)
* [ldatuning 0.2.0](https://cran.r-project.org/web/packages/ldatuning/index.html)

### python
* [numPy 1.14.0](https://www.scipy.org/scipylib/download.html)
* [guidedLDA 2.0.0.dev22](https://github.com/vi3k6i5/GuidedLDA)
* [lda 1.0.5](https://pypi.org/project/lda/)
* [gensim 3.2.0](https://radimrehurek.com/gensim/)
* [nltk 3.2.5](https://www.nltk.org/install.html)
***
## License

This project has no other license restrictions than from the packages and
third party code-snippets used. For detailed license information please
have a look at the acknowledgments below.

## Acknowledgments

* GuidedLDA: Github: [vi3k6i5](https://github.com/vi3k6i5/GuidedLDA)
* GuidedLDA: Article: [Vikash Singh](https://medium.freecodecamp.org/how-we-changed-unsupervised-lda-to-semi-supervised-guidedlda-e36a95f3a164)
* LDADE: paper by Agrawal, Menzies et al.: [What is Wrong with Topic Modeling?](https://arxiv.org/abs/1608.08176)
* LDADE: used pseudocode: [Amritanshu Agrawal](https://github.com/amritbhanu/LDADE-package)
* Inspiration for topic model diagnostics, topic exclusivity: [mallet](http://mallet.cs.umass.edu/diagnostics.php)
* LDA Tuning, performance metrics to determine topic number, by [Murzintcev Nikita](https://cran.r-project.org/web/packages/ldatuning/vignettes/topics.html)
* Cross Perplexity, adapted algorithm from [Murzintcev Nikita](http://freerangestats.info/blog/2017/01/05/topic-model-cv)
* Python topic modeling: [Radim Rehurek](https://radimrehurek.com/gensim/)
